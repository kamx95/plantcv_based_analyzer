#!/usr/bin/python
import cv2
import numpy as np
import argparse
from plantcv import plantcv as pcv
import plant_analyzer as pa
import os.path


# Parse command-line arguments
def options():
    parser = argparse.ArgumentParser(description="Imaging processing with opencv")
    parser.add_argument("-i", "--image", help="Input image file.", required=True)
    parser.add_argument("-o", "--outdir", help="Output directory for image files.", required=False)
    parser.add_argument("-r", "--result", help="result file.", required=False, default=False)
    parser.add_argument("-w", "--writeimg", help="write out images.", action="store_true")
    parser.add_argument("-D", "--debug", help="Turn on debug, prints intermediate images.", default=None)
    parser.add_argument("-e", "--exgr", help="Turn on segmentation using ExGR index", default=False,
                        action="store_true")
    return parser.parse_args()


def _draw_roi(img, roi_contour, filename):
    """Draw an ROI
    :param img: numpy.ndarray
    :param roi_contour: list
    :param filename: str
    """
    # Make a copy of the reference image
    ref_img = np.copy(img)
    # If the reference image is grayscale convert it to color
    if len(np.shape(ref_img)) == 2:
        ref_img = cv2.cvtColor(ref_img, cv2.COLOR_GRAY2BGR)
    # Draw the contour on the reference image
    cv2.drawContours(ref_img, roi_contour, -1, (255, 0, 0), 5)
    pcv.print_image(ref_img, os.path.join(pcv.params.debug_outdir, filename[:-4] + "_roi.png"))


def get_corrections(filename, img):
    date = filename[-12:-4]
    plant = filename[0]
    w_section, h_section, y_offset, geom_correction = None, None, None, None
    if '20180614' <= date < '20180816':
        h_section, w_section, y_offset, geom_correction = {'S': (0.5, 0.3, 300, 1),
                                                           'T': (0.5, 0.3, 200, 1),
                                                           'B': (0.5, 0.35, 0, 1)}[plant]
    if '20180816' <= date < '20180830':
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        h_section, w_section, y_offset, geom_correction = {'S': (0.35, 0.5, 0, 1),
                                                           'T': (0.3, 0.5, 200, 1),
                                                           'B': (0.5, 0.5, 250, 1)}[plant]
    if '20180830' <= date:
        img = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        h_section, w_section, y_offset, geom_correction = {'S': (0.3, 0.35, 250, 1.45),
                                                           'T': (0.3, 0.4, 200, 1.45),
                                                           'B': (0.4, 0.5, 300, 1.5)}[plant]

    height, width = img.shape[:2]
    ix = int(width * (0.5 - w_section))
    iy = int(max(height * (0.5 - h_section) + y_offset, 0))
    w = int(width * 2 * w_section)
    h = int(min(height * 2 * h_section, height - iy))

    return img, (ix, iy, h, w), geom_correction


def main():
    args = options()
    img, path, filename = pcv.readimage(args.image)
    pcv.params.debug = args.debug
    pcv.params.debug_outdir = args.outdir
    img, roi_xyhw, geom_correction = get_corrections(filename, img)

    img_blur = pcv.gaussian_blur(img, (9, 9))
    if args.exgr:
        raw_mask = pa.exgr_binary_mask(img_blur)
    else:
        green_magenta = pcv.rgb2gray_lab(img_blur, 'a')
        raw_mask = pcv.threshold.binary(green_magenta, 120, 255, 'dark')

    plant_mask_blur = pcv.median_blur(raw_mask, 9)
    plant_mask_blur = pa.remove_small_obj(plant_mask_blur, 2000)

    if pcv.params.debug == 'plot':
        pcv.plot_image(plant_mask_blur)

    obj_cnt, obj_hierarchy = pcv.find_objects(img, plant_mask_blur)
    roi_contour, roi_hierarchy = pcv.roi.rectangle(*roi_xyhw, img=img)

    # keep contours partially in roi
    kept_cnt, hierarchy, new_mask, obj_area = pcv.roi_objects(img, 'cutto',
                                                              roi_contour, roi_hierarchy,
                                                              obj_cnt, obj_hierarchy)
    # merge contours
    obj, mask = pcv.object_composition(img, kept_cnt, hierarchy)

    ############### Analysis ################

    outfile = False
    if args.writeimg:
        outfile = args.outdir + "/" + filename

    # Find shape properties, output shape image (optional)
    shape_header, shape_data, shape_img = pa.analyze_object(img, obj, mask, outfile, geom_correction)

    # Determine color properties: Histograms, Color Slices and Pseudocolored Images
    color_header, color_data, color_img = pa.analyze_color(img, new_mask, 256, 'all', 'm', 'img', outfile)

    # Histogram from 3 circle regions - 'a' channel from 'lab'
    if obj is None:
        circle_regions_header, circle_regions_data = None, None
    else:
        circle_regions_header, circle_regions_data = pa.histogram_from_circle_regions(img, obj, new_mask, 'bottom')

    # Segment image with watershed function
    if obj is None:
        watershed_header, watershed_data, analysis_images = None, None, None
    else:
        watershed_header, watershed_data, analysis_images = pa.leafs_analyzer(img, mask, 25, outfile)

    # Write shape and color data to results file
    if args.result is not False:
        result = open(args.result, "a")
        result.write('\t'.join(map(str, shape_header)))
        result.write("\n")
        result.write('\t'.join(map(str, shape_data)))
        result.write("\n")

        if obj is not None:
            result.write('\t'.join(map(str, circle_regions_header)))
            result.write("\n")
            result.write('\t'.join(map(str, circle_regions_data)))
            result.write("\n")

            result.write('\t'.join(map(str, watershed_header)))
            result.write("\n")
            result.write('\t'.join(map(str, watershed_data)))
            result.write("\n")

            result.write('\t'.join(map(str, color_header)))
            result.write("\n")
            result.write('\t'.join(map(str, color_data)))
            result.write("\n")
            for row in color_img:
                result.write('\t'.join(map(str, row)))
                result.write("\n")

        result.close()


if __name__ == '__main__':
    main()
