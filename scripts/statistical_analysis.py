import pandas as pd
import argparse
from utils import DbReader, StatisticalAnalyzer


def parse_args():
    parser = argparse.ArgumentParser(description="Processing features from SQLite database")
    parser.add_argument("-db", "--database", help="Input database file (dir)", required=True)
    parser.add_argument("-p", "--plant", help="Plant type [B, M, P, S, T]", required=True)
    parser.add_argument("-o", "--out", help=".xlsx filename", required=True)
    parser.add_argument("-hn", "--hist_n", help="Amount of histogram bins", required=False, default=5)
    parser.add_argument("-pos", "--position", help='Plant position [G, B]', required=True)
    return parser.parse_args()


def main():

    args = parse_args()
    db_parser = DbReader(args.database)

    n_data_frame = db_parser.get_dataframe(treatments=["001", "002", "000", "003"], plant_type=args.plant,
                                           position=args.position, hist_chunks_n=args.hist_n)
    p_data_frame = db_parser.get_dataframe(treatments=["010", "020", "000", "030"], plant_type=args.plant,
                                           position=args.position, hist_chunks_n=args.hist_n)
    k_data_frame = db_parser.get_dataframe(treatments=["100", "200", "000", "300"], plant_type=args.plant,
                                           position=args.position, hist_chunks_n=args.hist_n)

    subset_len = 3

    n_r2_rol_mean, n_corr_matrix, n_cfs_max = StatisticalAnalyzer.analyze_dataframe(
        n_data_frame, plot_name=args.position + "_nitrogen", subset_len=subset_len)
    p_r2_rol_mean, p_corr_matrix, p_cfs_max = StatisticalAnalyzer.analyze_dataframe(
        p_data_frame, plot_name=args.position + "phosphorus", subset_len=subset_len)
    k_r2_rol_mean, k_corr_matrix, k_cfs_max = StatisticalAnalyzer.analyze_dataframe(
        k_data_frame, plot_name=args.position + "_potassium", subset_len=subset_len)

    # exporting .xlsx
    writer = pd.ExcelWriter(args.out)
    n_r2_rol_mean.to_excel(writer, 'Nitrogen R^2')
    n_corr_matrix.to_excel(writer, 'Nitrogen Corr')
    n_cfs_max.to_excel(writer, 'Nitrogen CFS')
    p_r2_rol_mean.to_excel(writer, 'Phosphorus R^2')
    p_corr_matrix.to_excel(writer, 'Phosphorus Corr')
    p_cfs_max.to_excel(writer, 'Phosphorus CFS')
    k_r2_rol_mean.to_excel(writer, 'Potassium R^2')
    k_corr_matrix.to_excel(writer, 'Potassium Corr')
    k_cfs_max.to_excel(writer, 'Potassium CFS')
    writer.save()


if __name__ == '__main__':
    main()
