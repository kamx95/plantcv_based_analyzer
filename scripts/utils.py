from __future__ import print_function
import sqlite3 as sql
from ast import literal_eval as str_eval
from scipy.stats import linregress as lin_reg
from itertools import combinations
import matplotlib.pyplot as plt
import numpy as np
from pandas import DataFrame, to_datetime
import os
from multiprocessing import Pool


class DbReader(object):

    def __init__(self, database):
        self.conn = sql.connect(database)
        self.db = self.conn.cursor()

    def __del__(self):
        self.conn.close()

    def _get_data(self, feature_list, treatment, plant_type, position):

        self.db.execute('''Select metadata.image_id, metadata.timestamp, %s From metadata 
        inner join features on metadata.image_id=features.image_id 
        where metadata.treatment="%s" and metadata.position="%s" and metadata.planttype="%s" 
        order by metadata.timestamp, metadata.id ASC;''' % (feature_list, treatment, position, plant_type))
        query_ans = self.db.fetchall()
        return query_ans

    def _get_signals(self, treatment, plant_type, position):
        self.db.execute('''Select signal.image_id, signal.channel_name, signal.[values] From metadata 
        inner join signal on metadata.image_id=signal.image_id 
        where metadata.treatment="%s" and metadata.position="%s" and metadata.planttype="%s" 
        order by signal.image_id, signal.channel_name ASC;''' % (treatment, position, plant_type))
        query_ans = self.db.fetchall()
        return query_ans

    def get_dataframe(self, treatments, plant_type, position, hist_chunks_n):

        features_labels = ['features.area', 'features.hull_area', 'features.solidity',
                           'features.perimeter', 'features.hull_vertices',
                           'features.estimated_object_count']

        hists_labels = ['features.hist_young', 'features.hist_normal', 'features.hist_old',
                        'features.leaf_hist_a']

        signals_labels = []  # downloaded from database

        t_to_f = {'000': 1.0,  # nominal
                  '001': 0.33, '002': 0.66, '003': 1.33,  # N
                  '010': 0.33, '020': 0.66, '030': 1.33,  # P
                  '100': 0.33, '200': 0.66, '300': 1.33}  # K

        plant_data = []

        print("Loading data from db:")
        for n, treat in enumerate(treatments):  # collecting data from db
            print(str(1 + n) + "/" + str(len(treatments)) + " ", end='')

            # data from all dates for specified treatment
            raw_data = self._get_data(", ".join(features_labels), treat, plant_type, position)
            hists_data_raw = self._get_data(", ".join(hists_labels), treat, plant_type, position)
            signals_data_raw = self._get_signals(treat, plant_type, position)
            # change [0-255] bins to 'hist_chunks_n' bins by summing chunks
            # then normalize = divide by total sum
            for i, line in enumerate(hists_data_raw):
                if line[2] == '0':
                    continue
                plant_data.append([t_to_f[treat]])
                # single-valued features
                plant_data[-1].extend(map(float, raw_data[i][1:]))  # [1:] without plant_id

                # custom histograms
                for hist in line[2:]:
                    hist = str_eval(hist)
                    chunks = list(map(np.sum, np.array_split(hist, hist_chunks_n)))
                    chunks_sum = np.sum(chunks)

                    if chunks_sum > 0:
                        chunks = np.array(chunks, dtype=np.float32)
                        chunks /= float(chunks_sum)
                    plant_data[-1].extend(chunks)

                # plantcv signal data
                signal_data = [x[1:] for x in signals_data_raw if x[0] == line[0]]
                for ch_name, values in signal_data:
                    if ch_name not in signals_labels:
                        signals_labels.append(ch_name)
                    temp_sig = str_eval(values)
                    chunks = list(map(np.sum, np.array_split(temp_sig, hist_chunks_n)))
                    chunks_sum = np.sum(chunks)

                    if chunks_sum > 0:
                        chunks = np.array(chunks)
                        chunks /= float(chunks_sum)
                    plant_data[-1].extend(chunks)

        print("\nData loaded")
        column_names = ['treat', 'date'] + [x[9:] for x in features_labels]
        hists_names = [x[9:] for x in hists_labels] + signals_labels
        hists_names = [x + '_' + str(i % hist_chunks_n) for i, x in
                       enumerate(np.array(hists_names).repeat(hist_chunks_n))]
        column_names += hists_names

        plant_data_frame = DataFrame(plant_data, None, column_names)
        plant_data_frame['date'] = to_datetime(plant_data_frame['date'], format="%Y%m%d")
        plant_data_frame.set_index(["treat", "date"], append=True, inplace=True)

        return plant_data_frame.sort_index()


def columns_sum_wrapper(args):  # Has to be defined in the top level of module
    return StatisticalAnalyzer.columns_sum(*args)


class StatisticalAnalyzer(object):

    @staticmethod
    def calc_r2(data_frame, remove_min_max=False):
        if remove_min_max:
            temp = data_frame.drop(data_frame.idxmax())
            temp = temp.drop(temp.idxmin())
            reg_result = lin_reg(np.array(temp.reset_index('treat')))
        else:
            reg_result = lin_reg(np.array(data_frame.reset_index('treat')))
        return reg_result.rvalue ** 2

    @staticmethod
    def columns_sum(data_frame, index, columns):
        return data_frame.loc[index][columns].sum()

    @staticmethod
    def cfs_criterion_denom(corr_d_frame, feat_list, k):
        temp_sum = 0
        for cb in combinations(feat_list, 2):
            temp_sum += abs(corr_d_frame.loc[cb])
        return np.sqrt(temp_sum * 2 + k)

    @staticmethod
    def add_window_index(data_frame, window_size):
        w_s = window_size - 1
        new_index = ['Invalid'] * w_s
        dates = data_frame.index.strftime("%Y-%m-%d")
        for i in range(w_s, len(dates)):
            new_index.append(str(dates[i - w_s]) + " - " + str(dates[i]))
        data_frame.insert(0, "range", new_index)
        data_frame.set_index("range", inplace=True)

    @staticmethod
    def linear_fit(features, date_range, data_frame, plot_title=None):
        mask1 = data_frame.index.get_level_values('date') >= date_range[0]
        mask2 = data_frame.index.get_level_values('date') <= date_range[1]
        data_between = data_frame[features].loc[mask1 & mask2]
        # ||b-ax||^2
        x_values = data_between.to_numpy()
        # padding one -> constant term in linear function
        x_values = np.pad(x_values, ((0, 0), (0, 1)), 'constant', constant_values=1)
        y_real = data_between.index.get_level_values('treat').to_numpy()
        beta = np.linalg.lstsq(x_values, y_real, rcond=None)[0]
        y_est = np.dot(x_values, beta)
        ss_res = np.sum((y_real - y_est) ** 2)
        ss_tot = np.sum((y_real - y_real.mean()) ** 2)
        adj_term = float(len(y_real) - 1) / (len(y_real) - len(beta) - 1)
        adj_r_squared = 1 - adj_term * ss_res / ss_tot

        if plot_title is not None:
            fig, ax = plt.subplots()
            ax.plot(y_real, y_real, 'ro', label='Real')
            ax.plot(y_real, y_est, 'b^', label='Estimated: ' + str(adj_r_squared)[:4])
            ax.legend()
            plt.xlabel('Real')
            plt.xlabel('Concentration')
            plt.title(str(features) + " " + str(date_range))
            if plot_title == 'print':
                plt.show()
            else:
                if not os.path.exists("plots"):
                    os.mkdir("plots")
                plt.savefig("plots/" + '_to_'.join(date_range).replace(' 00:00:00', '') + "_"
                            + plot_title + "_" + '&'.join(features) + ".png")
            plt.close()
        return adj_r_squared, ", ".join("%.4f" % x for x in beta)

    @staticmethod
    def analyze_dataframe(data_frame, window_len=4, subset_len=3, threads=8, plot_name=None):

        print("Calculating R^2 value")
        r2 = data_frame.groupby('date').agg(StatisticalAnalyzer.calc_r2)
        r2_rol_mean = r2.rolling(window_len).mean()

        # Generating new index (ends of rolling window)
        StatisticalAnalyzer.add_window_index(r2_rol_mean, window_len)

        # Max within windows
        r2_rol_mean = DataFrame({'range': r2_rol_mean.idxmax(), 'R^2': r2_rol_mean.max()}) \
            .sort_values('R^2', ascending=False)

        # Person Correlation
        corr_matrix = data_frame.corr(method='pearson')

        # Correlation feature selection
        column_labels = r2.columns.get_values()
        column_comb = list(map(list, combinations(column_labels, subset_len)))
        column_names = list(map(lambda y: str(y)[1:-1], column_comb))

        print("Calculating CFS matrix")
        mp_pool = Pool(threads)
        cfs_nominator = DataFrame(data=[mp_pool.map(columns_sum_wrapper,
                                                    [(r2, i, c) for c in column_comb]) for i in r2.index],
                                  index=r2.index, columns=column_names)
        cfs_denominator = [StatisticalAnalyzer.cfs_criterion_denom(corr_matrix, f_list, subset_len)
                           for f_list in column_comb]

        cfs = cfs_nominator / cfs_denominator
        print("CFS matrix calculated")

        # CFS rolling window
        cfs_rol = cfs.rolling(window_len).mean()

        # Generating new index (ends of rolling window)
        StatisticalAnalyzer.add_window_index(cfs_rol, window_len)

        # Best feature subset for each window
        cfs_max = DataFrame({'Features': cfs_rol.idxmax(axis=1), 'CFS': cfs_rol.max(axis=1)}).dropna()
        cfs_max.index.name = 'Range'

        print("Fitting lin functions")
        fit_data = [StatisticalAnalyzer.linear_fit(list(str_eval(row.Features)), row.Index.split(' - '), data_frame,
                               plot_name) for row in cfs_max.itertuples()]

        # regression params: the most right value is the const therm
        cfs_max[['Adj R^2', 'Regression params']] = DataFrame(fit_data, index=cfs_max.index)
        return r2_rol_mean, corr_matrix, cfs_max
