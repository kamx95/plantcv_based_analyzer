# Functions from PlantCV with small edits

import cv2
import os
import numpy as np
from scipy import ndimage as ndi
from skimage.feature import peak_local_max
from skimage.morphology import watershed
from plantcv.plantcv import print_image, plot_image, \
    apply_mask, color_palette, params, rgb2gray, median_blur,\
    fatal_error, plot_colorbar
from matplotlib import pyplot as plt


def find_bottom_of_plant(mask, search_width=300):
    height, width = mask.shape[:2]
    for y in range(height-1, 0, -1):
        for x in range(int(width/2)-search_width/2, int(width/2)+search_width/2):
            if mask[y, x] > 0:
                return y


def leafs_analyzer(rgb_img, mask, distance=10, filename=False):
    """Uses the watershed algorithm to detect boundary of objects. Needs a marker file which specifies area which is
       object (white), background (grey), unknown area (black).

    Inputs:
    rgb_img             = image to perform watershed on needs to be 3D (i.e. np.shape = x,y,z not np.shape = x,y)
    mask                = binary image, single channel, object in white and background black
    distance            = min_distance of local maximum
    filename            = if user wants to output analysis images change filenames from false

    Returns:
    watershed_header    = shape data table headers
    watershed_data      = shape data table values
    analysis_images     = list of output images

    :param rgb_img: numpy.ndarray
    :param mask: numpy.ndarray
    :param distance: int
    :param filename: str
    :return watershed_header: list
    :return watershed_data: list
    :return analysis_images: list
    """

    if cv2.__version__[0] == '2':
        dist_transform = cv2.distanceTransform(mask, cv2.cv.CV_DIST_L2, maskSize=0)
    else:
        dist_transform = cv2.distanceTransformWithLabels(mask, cv2.DIST_L2, maskSize=0)[0]

    localMax = peak_local_max(dist_transform, indices=False, min_distance=distance, labels=mask)

    markers = ndi.label(localMax, structure=np.ones((3, 3)))[0]
    labels = watershed(-dist_transform, markers, mask=mask)

    img1 = np.copy(rgb_img)

    # Convert the input BGR image to LAB colorspace
    lab = cv2.cvtColor(rgb_img, cv2.COLOR_BGR2LAB)
    # Split LAB channels
    l, a, b = cv2.split(lab)
    img_green = np.copy(l)
    leaf_hist_a = 256 * [0]

    label_list = np.unique(labels)
    for x in label_list:
        rand_color = color_palette(len(label_list))
        img1[labels == x] = rand_color[x]
        avg = np.average(l[labels == x])
        img_green[labels == x] = avg
        if x != 0:  # not counting background
            leaf_hist_a[int(min(avg, 255))] += 1

    img2 = apply_mask(img1, mask, 'black')
    img_green_p = cv2.applyColorMap(img_green, cv2.COLORMAP_SUMMER)
    img_green_p = apply_mask(img_green_p, mask, 'black')
    joined = np.concatenate((img_green_p, img2, rgb_img), axis=1)

    estimated_object_count = len(np.unique(markers)) - 1

    analysis_images = []
    if filename is not False:
        out_file = str(filename[0:-4]) + '_watershed.jpg'
        print_image(joined, out_file)
        analysis_images.append(['IMAGE', 'watershed', out_file])

    watershed_header = (
        'HEADER_WATERSHED',
        'estimated_object_count',
        'leaf_hist_a'  # lab
    )

    watershed_data = (
        'WATERSHED_DATA',
        estimated_object_count,
        leaf_hist_a
    )

    if params.debug == 'print':
        print_image(dist_transform, os.path.join(params.debug_outdir, str(params.device) + '_watershed_dist_img.png'))
        print_image(joined, os.path.join(params.debug_outdir, str(params.device) + '_watershed_img.png'))
    elif params.debug == 'plot':
        plot_image(dist_transform, cmap='gray')
        plot_image(joined)

        plt.title('Leaf greenness histogram')
        plt.plot(leaf_hist_a, color='green')
        plt.xlim([50, 200])
        plt.show()

    return watershed_header, watershed_data, analysis_images


def pot_center(color_img, outfile=False):
    g_img = rgb2gray(color_img)
    g_img = median_blur(g_img, 5)
    params.device += 1
    circles = cv2.HoughCircles(g_img, cv2.HOUGH_GRADIENT, 8, 500, param1=220, param2=1900, minRadius=250)

    circles = np.uint16(np.around(circles))

    if params.debug == 'plot' or outfile is not False:
        c_img = color_img.copy()
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(c_img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(c_img, (i[0], i[1]), 2, (0, 0, 255), 3)

        if outfile is not False:
            print_image(c_img, str(outfile[0:-4]) + 'pot.jpg')
        if params == 'plot':
            plot_image(c_img)
    return circles[0, 0, :2]


def center_hull_distance(color_img, center, hull):
    dist_list = [[], []]
    img_rect = (0, 0, color_img.shape[0], color_img.shape[1])
    for x in hull:
        r, p1, p2 = cv2.clipLine(img_rect, center, tuple(x[0]))
        dist_list[0].append(int(np.linalg.norm(np.array(center)-np.array(tuple(x[0])))))
        dist_list[1].append(p2)
    return dist_list


def analyze_object(img, obj, mask, filename=False, correction=1):
    """Outputs numeric properties for an input object (contour or grouped contours).

    Inputs:
    img             = RGB or grayscale image data for plotting
    obj             = single or grouped contour object
    mask            = Binary image to use as mask for moments analysis
    filename        = False or image name. If defined print image

    Returns:
    shape_header    = shape data table headers
    shape_data      = shape data table values
    analysis_images = list of output images

    :param img: numpy.ndarray
    :param obj: list
    :param mask: numpy.ndarray
    :param filename: str
    :param correction: float
    :return shape_header: list
    :return shape_data: list
    :return analysis_images: list
    """

    params.device += 1

    # Valid objects can only be analyzed if they have >= 5 vertices
    if obj is None or len(obj) < 5:
        return None, None, None

    ori_img = np.copy(img)
    # Convert grayscale images to color
    if len(np.shape(ori_img)) == 2:
        ori_img = cv2.cvtColor(ori_img, cv2.COLOR_GRAY2BGR)

    if len(np.shape(img)) == 3:
        ix, iy, iz = np.shape(img)
    else:
        ix, iy = np.shape(img)
    size1 = ix, iy

    # Check is object is touching image boundaries (QC)
    frame_background = np.zeros(size1, dtype=np.uint8)
    frame = frame_background + 1
    frame_contour, frame_heirarchy = cv2.findContours(frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[-2:]
    ptest = []
    vobj = np.vstack(obj)
    for i, c in enumerate(vobj):
        xy = tuple(c)
        pptest = cv2.pointPolygonTest(frame_contour[0], xy, measureDist=False)
        ptest.append(pptest)
    in_bounds = all(c == 1 for c in ptest)

    # Convex Hull
    hull = cv2.convexHull(obj)
    hull_vertices = len(hull)
    # Moments
    m = cv2.moments(mask, binaryImage=True)
    # Properties
    # Area
    area = m['m00']

    if area:
        # Convex Hull area
        hull_area = cv2.contourArea(hull)
        # Solidity
        solidity = 1
        if int(hull_area) != 0:
            solidity = area / hull_area
        # Perimeter
        perimeter = cv2.arcLength(obj, closed=True)
        # x and y position (bottom left?) and extent x (width) and extent y (height)
        x, y, width, height = cv2.boundingRect(obj)
        # Centroid (center of mass x, center of mass y)
        cmx, cmy = (m['m10'] / m['m00'], m['m01'] / m['m00'])
        # Ellipse
        center, axes, angle = cv2.fitEllipse(obj)
        major_axis = np.argmax(axes)
        minor_axis = 1 - major_axis
        major_axis_length = axes[major_axis]
        minor_axis_length = axes[minor_axis]
        eccentricity = np.sqrt(1 - (axes[minor_axis] / axes[major_axis]) ** 2)
        # hull_dist_list = center_hull_distance(img, (int(cmx), int(cmy)), hull)

    # Store Shape Data
    shape_header = [
        'HEADER_SHAPES',
        'area',
        'hull_area',
        'solidity',
        'perimeter',
        'width',
        'height',
        'longest_axis',
        'center_of_mass_x',
        'center_of_mass_y',
        'hull_vertices',
        'in_bounds',
        'ellipse_center_x',
        'ellipse_center_y',
        'ellipse_major_axis',
        'ellipse_minor_axis',
        'ellipse_angle',
        'ellipse_eccentricity'
    ]

    caliper_length = None  # no axis analysis

    shape_data = [
        'SHAPES_DATA',
        area*correction**2,
        hull_area*correction**2,
        solidity,
        perimeter*correction,
        width*correction,
        height*correction,
        caliper_length,
        cmx,
        cmy,
        hull_vertices,
        in_bounds,
        center[0],
        center[1],
        major_axis_length*correction,
        minor_axis_length*correction,
        angle,
        eccentricity
    ]

    analysis_images = []

    # Draw properties
    if area and filename:
        cv2.drawContours(ori_img, obj, -1, (255, 0, 0), 5)
        cv2.drawContours(ori_img, [hull], -1, (0, 0, 255), 5)
        # cv2.line(ori_img, (x, y), (x + width, y), (0, 0, 255), 5)
        # cv2.line(ori_img, (int(cmx), y), (int(cmx), y + height), (0, 0, 255), 5)
        cv2.circle(ori_img, (int(cmx), int(cmy)), 10, (0, 0, 255), 5)
        # cv2.line(ori_img, (tuple(caliper_transpose[caliper_length - 1])), (tuple(caliper_transpose[0])), (0, 0, 255), 5)
        # vhull = np.vstack(hull)
        # for pt in vhull:
        #     cv2.line(ori_img, (int(cmx), int(cmy)), tuple(pt), (0, 0, 255), 5)

        # Output images with convex hull, extent x and y
        out_file = str(filename[0:-4]) + '_shapes.jpg'
        out_file1 = str(filename[0:-4]) + '_mask.jpg'

        print_image(ori_img, out_file)
        analysis_images.append(['IMAGE', 'shapes', out_file])

        print_image(mask, out_file1)
        analysis_images.append(['IMAGE', 'mask', out_file1])

    else:
        pass

    if params.debug is not None:
        cv2.drawContours(ori_img, obj, -1, (255, 0, 0), 5)
        cv2.drawContours(ori_img, [hull], -1, (0, 0, 255), 5)
        # cv2.line(ori_img, (x, y), (x + width, y), (0, 0, 255), 5)
        # cv2.line(ori_img, (int(cmx), y), (int(cmx), y + height), (0, 0, 255), 5)
        cv2.circle(ori_img, (int(cmx), int(cmy)), 10, (0, 0, 255), 5)
        # cv2.line(ori_img, (tuple(caliper_transpose[caliper_length - 1])), (tuple(caliper_transpose[0])), (0, 0, 255), 5)
        # vhull = np.vstack(hull)
        # for pt in vhull:
        #     cv2.line(ori_img, (int(cmx), int(cmy)), tuple(pt), (0, 0, 255), 5)

        if params.debug == 'print':
            print_image(ori_img, os.path.join(params.debug_outdir, str(params.device) + '_shapes.jpg'))
        elif params.debug == 'plot':
            if len(np.shape(img)) == 3:
                plot_image(ori_img)
            else:
                plot_image(ori_img, cmap='gray')

    return shape_header, shape_data, analysis_images


def analyze_color(rgb_img, mask, bins, hist_plot_type=None, pseudo_channel='v',
                  pseudo_bkg='img', filename=False):
    """Analyze the color properties of an image object

    Inputs:
    rgb_img          = RGB image data
    mask             = Binary mask made from selected contours
    hist_plot_type   = 'None', 'all', 'rgb', 'lab' or 'hsv'
    color_slice_type = 'None', 'rgb', 'hsv' or 'lab'
    pseudo_channel   = 'None', 'l', 'm' (green-magenta), 'y' (blue-yellow), h','s', or 'v', creates pseduocolored image
                       based on the specified channel
    pseudo_bkg       = 'img' => channel image, 'white' => white background image, 'both' => both img and white options
    filename         = False or image name. If defined print image

    Returns:
    hist_header      = color histogram data table headers
    hist_data        = color histogram data table values
    analysis_images  = list of output images

    :param rgb_img: numpy.ndarray
    :param mask: numpy.ndarray
    :param bins: int
    :param hist_plot_type: str
    :param pseudo_channel: str
    :param pseudo_bkg: str
    :param filename: str
    :return hist_header: list
    :return hist_data: list
    :return analysis_images: list
    """
    params.device += 1

    masked = cv2.bitwise_and(rgb_img, rgb_img, mask=mask)
    b, g, r = cv2.split(masked)
    lab = cv2.cvtColor(masked, cv2.COLOR_BGR2LAB)
    l, m, y = cv2.split(lab)
    hsv = cv2.cvtColor(masked, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    # Color channel dictionary
    norm_channels = {"b": np.divide(b, (256 / bins)).astype(np.uint8),
                     "g": np.divide(g, (256 / bins)).astype(np.uint8),
                     "r": np.divide(r, (256 / bins)).astype(np.uint8),
                     "l": np.divide(l, (256 / bins)).astype(np.uint8),
                     "m": np.divide(m, (256 / bins)).astype(np.uint8),
                     "y": np.divide(y, (256 / bins)).astype(np.uint8),
                     "h": np.divide(h, (256 / bins)).astype(np.uint8),
                     "s": np.divide(s, (256 / bins)).astype(np.uint8),
                     "v": np.divide(v, (256 / bins)).astype(np.uint8)
                     }

    # Histogram plot types
    hist_types = {"all": ("b", "g", "r", "l", "m", "y", "h", "s", "v"),
                  "rgb": ("b", "g", "r"),
                  "lab": ("l", "m", "y"),
                  "hsv": ("h", "s", "v")}

    # If the user-input pseudo_channel is not None and is not found in the list of accepted channels, exit
    if pseudo_channel is not None and pseudo_channel not in norm_channels:
        fatal_error("Pseudocolor channel was " + str(pseudo_channel) +
                    ', but can only be one of the following: None, "l", "m", "y", "h", "s" or "v"!')
    # If the user-input pseudocolored image background is not in the accepted input list, exit
    if pseudo_bkg not in ["white", "img", "both"]:
        fatal_error("The pseudocolored image background was " + str(pseudo_bkg) +
                    ', but can only be one of the following: "white", "img", or "both"!')
    # If the user-input histogram color-channel plot type is not in the list of accepted channels, exit
    if hist_plot_type is not None and hist_plot_type not in hist_types:
        fatal_error("The histogram plot type was " + str(hist_plot_type) +
                    ', but can only be one of the following: None, "all", "rgb", "lab", or "hsv"!')
    histograms = {
        "b": {"label": "blue", "graph_color": "blue",
              "hist": cv2.calcHist([norm_channels["b"]], [0], mask, [bins], [0, (bins - 1)])},
        "g": {"label": "green", "graph_color": "forestgreen",
              "hist": cv2.calcHist([norm_channels["g"]], [0], mask, [bins], [0, (bins - 1)])},
        "r": {"label": "red", "graph_color": "red",
              "hist": cv2.calcHist([norm_channels["r"]], [0], mask, [bins], [0, (bins - 1)])},
        "l": {"label": "lightness", "graph_color": "dimgray",
              "hist": cv2.calcHist([norm_channels["l"]], [0], mask, [bins], [0, (bins - 1)])},
        "m": {"label": "green_magenta", "graph_color": "magenta",
              "hist": cv2.calcHist([norm_channels["m"]], [0], mask, [bins], [0, (bins - 1)])},
        "y": {"label": "blue_yellow", "graph_color": "yellow",
              "hist": cv2.calcHist([norm_channels["y"]], [0], mask, [bins], [0, (bins - 1)])},
        "h": {"label": "hue", "graph_color": "blueviolet",
              "hist": cv2.calcHist([norm_channels["h"]], [0], mask, [bins], [0, (bins - 1)])},
        "s": {"label": "saturation", "graph_color": "cyan",
              "hist": cv2.calcHist([norm_channels["s"]], [0], mask, [bins], [0, (bins - 1)])},
        "v": {"label": "value", "graph_color": "orange",
              "hist": cv2.calcHist([norm_channels["v"]], [0], mask, [bins], [0, (bins - 1)])}
    }

    hist_data_b = np.squeeze(histograms["b"]["hist"], axis=1).tolist()
    hist_data_g = np.squeeze(histograms["g"]["hist"], axis=1).tolist()
    hist_data_r = np.squeeze(histograms["r"]["hist"], axis=1).tolist()
    hist_data_l = np.squeeze(histograms["l"]["hist"], axis=1).tolist()
    hist_data_m = np.squeeze(histograms["m"]["hist"], axis=1).tolist()
    hist_data_y = np.squeeze(histograms["y"]["hist"], axis=1).tolist()
    hist_data_h = np.squeeze(histograms["h"]["hist"], axis=1).tolist()
    hist_data_s = np.squeeze(histograms["s"]["hist"], axis=1).tolist()
    hist_data_v = np.squeeze(histograms["v"]["hist"], axis=1).tolist()

    binval = np.arange(0, bins)
    bin_values = [l for l in binval]

    # Store Color Histogram Data
    hist_header = [
        'HEADER_HISTOGRAM',
        'bin_number',
        'bin_values',
        'blue',
        'green',
        'red',
        'lightness',
        'green_magenta',
        'blue_yellow',
        'hue',
        'saturation',
        'value'
    ]

    hist_data = [
        'HISTOGRAM_DATA',
        bins,
        bin_values,
        hist_data_b,
        hist_data_g,
        hist_data_r,
        hist_data_l,
        hist_data_m,
        hist_data_y,
        hist_data_h,
        hist_data_s,
        hist_data_v
    ]

    analysis_images = []

    if pseudo_channel is not None:
        analysis_images = _pseudocolored_image(norm_channels[pseudo_channel], bins, rgb_img, mask, pseudo_bkg,
                                               pseudo_channel, filename, analysis_images)

    if hist_plot_type is not None and filename:
        import matplotlib
        matplotlib.use('Agg', warn=False)
        from matplotlib import pyplot as plt

        # Create Histogram Plot
        for channel in hist_types[hist_plot_type]:
            plt.plot(histograms[channel]["hist"], color=histograms[channel]["graph_color"],
                     label=histograms[channel]["label"])
            plt.xlim([0, bins - 1])
            plt.legend()

        # Print plot
        fig_name = (str(filename[0:-4]) + '_' + str(hist_plot_type) + '_hist.svg')
        plt.savefig(fig_name)
        analysis_images.append(['IMAGE', 'hist', fig_name])
        if params.debug == 'print':
            fig_name = os.path.join(params.debug_outdir, str(params.device) + '_' + str(hist_plot_type) + '_hist.svg')
            plt.savefig(fig_name)
        plt.clf()

    return hist_header, hist_data, analysis_images


def _pseudocolored_image(histogram, bins, img, mask, background, channel, filename, analysis_images):
    """Pseudocolor image.

    Inputs:
    histogram       = a normalized histogram of color values from one color channel
    bins            = number of color bins the channel is divided into
    img             = input image
    mask            = binary mask image
    background      = what background image?: channel image (img) or white
    channel         = color channel name
    filename        = input image filename
    analysis_images = list of analysis image filenames

    Returns:
    analysis_images = list of analysis image filenames

    :param histogram: list
    :param bins: int
    :param img: numpy array
    :param mask: numpy array
    :param background: str
    :param channel: str
    :param filename: str
    :param analysis_images: list
    :return analysis_images: list
    """
    mask_inv = cv2.bitwise_not(mask)

    cplant = cv2.applyColorMap(histogram, colormap=2)
    cplant1 = cv2.bitwise_and(cplant, cplant, mask=mask)

    output_imgs = {"pseudo_on_img": {"background": "img", "img": None},
                   "pseudo_on_white": {"background": "white", "img": None}}

    if background == 'img' or background == 'both':
        # mask the background and color the plant with color scheme 'jet'
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        img_back = cv2.bitwise_and(img_gray, img_gray, mask=mask_inv)
        img_back3 = np.dstack((img_back, img_back, img_back))

        output_imgs["pseudo_on_img"]["img"] = cv2.add(cplant1, img_back3)

    if background == 'white' or background == 'both':
        # Get the image size
        if np.shape(img)[2] == 3:
            ix, iy, iz = np.shape(img)
        else:
            ix, iy = np.shape(img)
        size = ix, iy
        back = np.zeros(size, dtype=np.uint8)
        w_back = back + 255
        w_back3 = np.dstack((w_back, w_back, w_back))
        img_back3 = cv2.bitwise_and(w_back3, w_back3, mask=mask_inv)
        output_imgs["pseudo_on_white"]["img"] = cv2.add(cplant1, img_back3)

    if filename:
        for key in output_imgs:
            if output_imgs[key]["img"] is not None:
                fig_name_pseudo = str(filename[0:-4]) + '_' + str(channel) + '_pseudo_on_' + \
                                  output_imgs[key]["background"] + '.jpg'
                path = os.path.dirname(filename)
                print_image(output_imgs[key]["img"], fig_name_pseudo)
                analysis_images.append(['IMAGE', 'pseudo', fig_name_pseudo])
    else:
        path = "."

    if params.debug is not None:
        if params.debug == 'print':
            for key in output_imgs:
                if output_imgs[key]["img"] is not None:
                    print_image(output_imgs[key]["img"], os.path.join(params.debug_outdir, str(params.device) +
                                                                      "_" + output_imgs[key]["background"] +
                                                                      '_pseudocolor.jpg'))
            fig_name = 'VIS_pseudocolor_colorbar_' + str(channel) + '_channel.svg'
            if not os.path.isfile(os.path.join(params.debug_outdir, fig_name)):
                plot_colorbar(path, fig_name, bins)
        elif params.debug == 'plot':
            for key in output_imgs:
                if output_imgs[key]["img"] is not None:
                    plot_image(output_imgs[key]["img"])

    return analysis_images


def histogram_from_circle_regions(img, obj, mask, mode):

    size = np.shape(img)[:2]
    # Circle mask generating
    circle_cen, radius = cv2.minEnclosingCircle(obj)
    young_part_mask = np.zeros(size, dtype=np.uint8)
    normal_part_mask = np.zeros(size, dtype=np.uint8)
    old_part_mask = np.zeros(size, dtype=np.uint8)
    circle_cen = tuple(map(int, circle_cen))

    if mode == 'center':
        cv2.circle(old_part_mask, circle_cen, int(radius * 0.33), 255, -1)
        cv2.circle(normal_part_mask, circle_cen, int(radius * 0.66), 255, -1)
        cv2.circle(young_part_mask, circle_cen, int(radius), 255, -1)
    elif mode == 'bottom':
        cv2.circle(young_part_mask, circle_cen, int(radius), 255, -1)
        bottom_y = find_bottom_of_plant(mask)
        if bottom_y is not None:
            bottom_circle = (circle_cen[0], bottom_y - int(radius * 0.1))
            cv2.circle(old_part_mask, bottom_circle, int(radius * 0.33), 255, -1)
            cv2.circle(normal_part_mask, bottom_circle, int(radius * 0.66), 255, -1)

    else:
        fatal_error("Histogram from circle regions, unsupported mode: "+str(mode))

    cv2.subtract(young_part_mask, normal_part_mask, young_part_mask)
    cv2.subtract(normal_part_mask, old_part_mask, normal_part_mask)

    cv2.bitwise_and(young_part_mask, mask, young_part_mask)
    cv2.bitwise_and(normal_part_mask, mask, normal_part_mask)
    cv2.bitwise_and(old_part_mask, mask, old_part_mask)
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)

    hist_young = cv2.calcHist([lab], [1], young_part_mask, [256], [0, 256])
    hist_normal = cv2.calcHist([lab], [1], normal_part_mask, [256], [0, 256])
    hist_old = cv2.calcHist([lab], [1], old_part_mask, [256], [0, 256])

    hist_young = np.squeeze(hist_young, axis=1).tolist()
    hist_normal = np.squeeze(hist_normal, axis=1).tolist()
    hist_old = np.squeeze(hist_old, axis=1).tolist()

    if params.debug is not None:
        circle_img = np.copy(img)
        circle_img[old_part_mask == 255] = (24, 89, 13)
        circle_img[normal_part_mask == 255] = (24, 152, 13)
        circle_img[young_part_mask == 255] = (24, 236, 13)
        plot_image(circle_img)

        plt.title('Histogram for "a" channel from "lab"')
        plt.plot(hist_old, color='red', label='old')
        plt.plot(hist_normal, color='orange', label='normal')
        plt.plot(hist_young, color='yellow', label='young')
        plt.xlim([50, 150])
        plt.legend()
        plt.show()

    circle_regions_header = [
        'HEADER_CIRCLE',
        'hist_young',
        'hist_normal',
        'hist_old'
    ]
    circle_regions_data = [
        'CIRCLE_DATA',
        hist_young,
        hist_normal,
        hist_old
    ]
    return circle_regions_header, circle_regions_data


def exgr_binary_mask(img, fill=False, fill_range=1.0):
    b, g, r = cv2.split(img)

    b_n = b / 255.0
    g_n = g / 255.0
    r_n = r / 255.0

    rgb_sum = b_n + g_n + r_n
    rgb_sum[rgb_sum == 0] = 1

    b = b_n / rgb_sum
    g = g_n / rgb_sum
    r = r_n / rgb_sum

    exgr = 3*g - b - 2.4 * r

    if fill:
        fill_range = (fill_range,) * 3
        h, w = img.shape[:2]
        fill_mask = np.zeros((h + 2, w + 2), np.uint8)
        fill_flag = 4 | (255 << 8)  # 4 neighbor pixels, fill mask with 255
        fill_flag |= cv2.FLOODFILL_MASK_ONLY

        for x, y in np.argwhere(exgr > 0.0):
            cv2.floodFill(img, fill_mask, (y, x), None, fill_range, fill_range,
                          flags=fill_flag)
        return fill_mask[1:-1, 1:-1]
    else:
        retval, exgr_thr = cv2.threshold(exgr, 0.0, 255.0, cv2.THRESH_BINARY)
        return exgr_thr.astype(np.uint8)


def remove_small_obj(img, min_area):
    contours = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    contours = contours[1] if cv2.getVersionMajor() == 3 else contours[0]
    output = img.copy()

    for i, c in enumerate(contours):
        if cv2.contourArea(c) < min_area:
            cv2.drawContours(output, contours, i, 0, -1)

    return output
