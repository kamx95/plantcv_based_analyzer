#!/usr/bin/python
import argparse
from plantcv import plantcv as pcv
import plant_analyzer as pa


# Parse command-line arguments
def options():
    parser = argparse.ArgumentParser(description="Imaging processing with opencv")
    parser.add_argument("-i", "--image", help="Input image file.", required=True)
    parser.add_argument("-o", "--outdir", help="Output directory for image files.", required=False)
    parser.add_argument("-r", "--result", help="result file.", required=False, default=False)
    parser.add_argument("-w", "--writeimg", help="write out images.", action="store_true")
    parser.add_argument("-D", "--debug", help="Turn on debug, prints intermediate images.", default=None)
    parser.add_argument("-e", "--exgr", help="Turn on segmentation using ExGR index", default=False,
                        action="store_true")
    return parser.parse_args()


def main():
    args = options()
    img, path, filename = pcv.readimage(args.image)

    pcv.params.debug = args.debug
    pcv.params.debug_outdir = args.outdir

    img_blur = pcv.gaussian_blur(img, (9, 9))
    if args.exgr:
        raw_mask = pa.exgr_binary_mask(img_blur)
    else:
        green_magenta = pcv.rgb2gray_lab(img_blur, 'a')
        raw_mask = pcv.threshold.binary(green_magenta, 120, 255, 'dark')

    plant_mask_blur = pcv.median_blur(raw_mask, 9)
    plant_mask_blur = pa.remove_small_obj(plant_mask_blur, 2000)

    if pcv.params.debug == 'plot':
        pcv.plot_image(plant_mask_blur)

    obj_cnt, obj_hierarchy = pcv.find_objects(img, plant_mask_blur)
    height, width = img.shape[:2]
    roi_contour, roi_hierarchy = pcv.roi.rectangle(width/2 - int(width*0.3),
                                                   height/2 - int(height*0.4),
                                                   int(height*0.8), int(width*0.6),
                                                   img)

    # keep contours in roi
    kept_cnt, hierarchy, new_mask, obj_area = pcv.roi_objects(img, 'partial',
                                                              roi_contour, roi_hierarchy,
                                                              obj_cnt, obj_hierarchy)

    # merge contours
    obj, mask = pcv.object_composition(img, kept_cnt, hierarchy)

    ############### Analysis ################

    outfile = False
    if args.writeimg:
        outfile = args.outdir + "/" + filename

    # Find shape properties, output shape image (optional)
    shape_header, shape_data, shape_img = pa.analyze_object(img, obj, mask, outfile)

    # Determine color properties: Histograms, Color Slices and Pseudocolored Images
    color_header, color_data, color_img = pa.analyze_color(img, new_mask, 256, 'all', 'm', 'img', outfile)

    # Histogram from 3 circle regions - 'a' channel from 'lab'
    if obj is None:
        circle_regions_header, circle_regions_data = None, None
    else:
        circle_regions_header, circle_regions_data = pa.histogram_from_circle_regions(img, obj, new_mask, 'center')

    # Segment image with watershed function
    if obj is None:
        watershed_header, watershed_data, analysis_images = None, None, None
    else:
        watershed_header, watershed_data, analysis_images = pa.leafs_analyzer(img, mask, 25, outfile)

    # Write shape and color data to results file
    if args.result is not False:
        result = open(args.result, "a")
        result.write('\t'.join(map(str, shape_header)))
        result.write("\n")
        result.write('\t'.join(map(str, shape_data)))
        result.write("\n")

        if obj is not None:
            result.write('\t'.join(map(str, circle_regions_header)))
            result.write("\n")
            result.write('\t'.join(map(str, circle_regions_data)))
            result.write("\n")

            result.write('\t'.join(map(str, watershed_header)))
            result.write("\n")
            result.write('\t'.join(map(str, watershed_data)))
            result.write("\n")

            result.write('\t'.join(map(str, color_header)))
            result.write("\n")
            result.write('\t'.join(map(str, color_data)))
            result.write("\n")
            for row in color_img:
                result.write('\t'.join(map(str, row)))
                result.write("\n")

        result.close()


if __name__ == '__main__':
    main()
