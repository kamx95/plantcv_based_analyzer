## About
Automatic phenotyping by computer image processing for plant nutrient deficiency estimation (nitrogen, phosphorus, potassium).

## Dependencies:
* plantcv
    * argparse
    * opencv-python
    * matplotlib
    * numpy
    * pandas
    * python-dateutil
    * scikit-image
    * scipy
    * plotnine
    * setuptools
    * sqlite3
* tqdm
* openpyxl

## Scripts
### Image processing pipelines:
Extracting features (geometrical and colour-based) from images and saving them to SQLite database.
- VIS-pipeline-top-view.py
- VIS-pipeline-side-view.py

Usage:
```
VIS-pipeline-top-view.py -i Photos\S_003_4_G_20180816.jpg -D plot -o VIS_TEST --write_img
```

### Job builer:
- plantcv-pipeline-custom.py

Usage:
```
plantcv-pipeline-custom.py -d Photos -p scripts\VIS-pipeline-top-view.py -i out_seler -a filename -t jpg -f planttype_treatment_id_position_timestamp -M planttype:S,position:G -s seler_topview.sqlite3 -T 4
```
### Statistical analysis:
Finding best feature subset using CFS criterion (https://en.wikipedia.org/wiki/Feature_selection#Correlation_feature_selection).
Genereting and scoring linear models for plant nutrient deficiency estimation in specified time range.
- statistical_analysis.py
- utils.py

Usage:
```
statistical_analysis.py -db "databases\seler_topview.sqlite3" -p "S" -o "celery_topview.xlsx" -pos "G"
```
